'''
Algoritmo de aproximacion al coloreo de un grafo
basado en el articulo http://dx.doi.org/10.1109/CONIELECOMP.2015.7086935

'''


import time
import sys
import igraph
from igraph import *
import cairo

verticesPath = []
verticesCompletos = []
matrizNoVecinos = []


def coTree(graph,spTree):
	gCT=Graph()

	gCT=graph - spTree

	return gCT

def construirSubGrafo(graph, verticesG):
	aristasGI = graph.get_edgelist()
	aristasN = []

	for aristaG in aristasGI:
		for nodo in verticesG:
			if nodo in aristaG:
				if aristaG not in aristasN:
					aristasN.append(aristaG)

	
	return aristasN

def depurarGrafo(graph, graphEdges, graphVertices, arrayDFS):
	global verticesPath

	sT = spanningTree(graph,arrayDFS)
	cT = coTree(graph,sT)

	aristas = cT.get_edgelist()

	#Se generan los caminos en el ST considerando las aristas de cT
	caminos = []
	for arista in aristas:
		root=arista[0]
		target=arista[1]
		caminos.extend(sT.get_shortest_paths(root,target))

	#El siguiente ciclo hace que se ordenene los vertices de caminos sin repetirse
	verticesCiclo = set()
	for posiciones in caminos:
		for numero in posiciones:
	 		verticesCiclo.add(numero)

	#Se recorre graphVertices para verificar que los vertices de verticesCiclo no se eliminen
	verticesEliminar = []
	for v in graphVertices:
		if v not in verticesCiclo:
			verticesEliminar.append(v)

	verticesPath.extend(verticesEliminar)

	grafoDepurado = Graph(eliminaNodos(graph,verticesEliminar))


	return grafoDepurado, list(set(arrayDFS) - set(verticesEliminar))

def dfsiter(graph, root):
    stack = [root]
    path = []
    
    visited = set(stack)
    while stack:
    	vertex = stack.pop()
        #yield vertex
        path.append(vertex)
        not_visited_neis = set(graph.neighbors(vertex)) - visited
        lista_not_visited_neis = list(not_visited_neis)
        lista_not_visited_neis.reverse()
        not_visited_neis = lista_not_visited_neis
        stack.extend(not_visited_neis)
        visited.update(not_visited_neis)

    return path

def eliminaNodos(graphI, vertices):
	aristasGI = graphI.get_edgelist()
	arregloAr = graphI.get_edgelist()
	
	for vEliminar in vertices:
		for aristaG in aristasGI:
			if vEliminar in aristaG:
				if aristaG in arregloAr:
					pos = arregloAr.index(aristaG)
					del arregloAr[pos]

		
	return arregloAr

def eliminaNodosAislados(graphI):
	'''
	Se obtienen las componentes del grafo, se recorren para obtener la longitud,
	si el valor regresado es cero significa que es un vertice aislado, por lo que
	sera agregado a un arreglo de vertices a borrar
	'''
	components = graphI.components()
	
	borrados = []
	for component in components:
		longitud = min(6, len(component)-1)
	
		if longitud == 0:
			valor= component.pop()
			borrados.append(valor)
	'''
	El siguiente ciclo recorrera el arreglo de vertices a borrar, de atras
	hacia adelante para mantener la integridad del grafo
	'''		
	for i in range(len(borrados)):
		vBorrar = borrados.pop() 
		graphI.delete_vertices(vBorrar) 

	return graphI

def eliminarVerticesGrafo(graph, verticesEliminar):
	for v in verticesEliminar:
		nAristas = []
		aristasG = graph.get_edgelist()
		for arista in aristasG:
			if v not in arista:
				nAristas.append(arista)
		graph = Graph(nAristas)

	return graph

def getIguales(arreglo, posValor):
	elemBuscar = arreglo[posValor]
	listaMax = []

	listaMax.append(posValor)
	for pos in range(len(arreglo)):
		if arreglo[pos] == elemBuscar and posValor != pos:
			#if pos not in valoresMaximos:
			listaMax.append(pos)

	return listaMax

def getMaximo(arreglo):
	#print 'arreglo', arreglo
	eleMax = max(arreglo)
	#print 'eleMax', eleMax

	if eleMax != -1:
		return arreglo.index(eleMax)
	else:
		return "-"

def getNoAticulationV(graph, vertices):
	

	#se toma el primer elemento de la primera arista, para considerarlo como pivote
	aristasG=graph.get_edgelist()
	pivote = aristasG[0][0]

	arrayDFS = dfsiter(graph,pivote)


	sT = spanningTree(graph,arrayDFS)
	cT = coTree(graph,sT)

	verticesCT = getVertices(cT.get_edgelist())
	aristasCT = cT.get_edgelist()

	gradosCT = cT.degree(verticesCT)
	

	#inicializar conteo de vertices que intervienen en caminos
	#for i in range(len(vertices)):
	#	contCaminos.append(0)

	contCaminos = [0 for i in range(len(vertices))]
	
	
	nodosCamino = []
	for posiciones in aristasCT:		
		nodosCamino.extend(sT.get_shortest_paths(posiciones[0],posiciones[1]))

	
	for path in nodosCamino:
		for v in path:
			pos = vertices.index(v)
			contCaminos[pos] = contCaminos[pos] + 1

	

	gradosCTac=[0 for i in range(len(vertices))]
	for pos in range(len(verticesCT)):
		v = verticesCT[pos]
		gradosCTac[vertices.index(v)] = gradosCT[pos]

	#print contCaminos
	#print gradosCTac

	while True:
		#print contCaminos
		posMax = getMaximo(contCaminos)
		
		if posMax != '-':
			posMaximoscontCaminos = obtenerMaxIguales(contCaminos,posMax)
			valoresMaximosgradosCT = obtenerValoresMaximos(posMaximoscontCaminos,gradosCTac)
			#print posMaximoscontCaminos	
			#print valoresMaximosgradosCT

			#encontrar un candidato, si hay mas de uno vertice en contCaminos con el mismo valor
			#recorrer candidato seleccionando el de mayor tamanio en valoresMaximosgradosCT
			posMax = getMaximo(valoresMaximosgradosCT)
			while posMax != '-':
				posCandidato = posMaximoscontCaminos[posMax]
				vArtCandidato = vertices[posCandidato]
				#print 'vArtCandidato', vArtCandidato

				if verificarCorteGrafo(vArtCandidato,graph):
					#print 'lo corta'
					valoresMaximosgradosCT[posMax] = -1
					#agregar - a los valores ContCaminos
					pv= posMaximoscontCaminos[posMax]
					contCaminos[pv] = -1
					posMax = getMaximo(valoresMaximosgradosCT)

				else:
					#print 'no lo corta'
					return vArtCandidato
					#sys.exit(0)
		else:
			return None

def getSubGrafos(graph, verticesGrafo):
	grafos = []
	nodoVistados = []
	#Se verifica que el vertice v no este en visitados para tomarlo como pivote y aplicar dfs 
	for v in verticesGrafo:
		if v not in nodoVistados:
			arrayDFS = dfsiter(graph,v)

			grafos.append(arrayDFS)

			nodoVistados.extend([element for element in arrayDFS if element not in nodoVistados])
			nodoVistados.sort()

	return grafos

def getTamanio(graph):
	#cadena como:Clustering with 18 elements and 1 clusters
	lista = graph.components().summary(verbosity=0).split(" ")
	return lista[2]

def getVertices(graphEdges):
	vertices = []
	for e in graphEdges:
		if e[0] not in vertices:
			vertices.append(e[0])
		elif e[1] not in vertices:
			vertices.append(e[1])

	vertices.sort()
	return vertices

def generarVerticesCompletos(vertices):
	global verticesCompletos
	nMax = max(vertices)

	verticesCompletos = [element for element in range(nMax+1)]

def generarMIS(graph, verticesG):
	global matrizNoVecinos
	MISES = []
	while (graph.is_bipartite() == False):
		noArticulacionV = getNoAticulationV(graph, verticesG)
		print 'noArticulacionV', noArticulacionV
		mis = []
		if noArticulacionV != None:
			#print 'verticesG', verticesG
			mis.append(noArticulacionV)
			noVecinosOrdenados = ordenarVecinosGrafo(graph,matrizNoVecinos[verticesG.index(noArticulacionV)])
			print noVecinosOrdenados

			while(noVecinosOrdenados):
			
				ve = noVecinosOrdenados[0]
				if verificarCorteGrafo(ve,graph) == False:
					mis.append(ve)
					vecinos=graph.neighbors(ve)
				
					print 've', ve, vecinos
				
					noVecinosOrdenados = list(set(noVecinosOrdenados) - set(vecinos))
					#noVecinosOrdenados = restarArreglos(noVecinosOrdenados,vecinos)
					print 'noVecinosOrdenados d', noVecinosOrdenados
					noVecinosOrdenados.remove(ve)
				else:
					noVecinosOrdenados.remove(ve)
		print mis
		MISES.append(mis)
		#ELIMINIAR mis DEL GRAFO Y ACTUALZIAR VERTICES
		graph = eliminarVerticesGrafo(graph,mis)
		verticesG = getVertices(graph.get_edgelist())
		llenarMatriz(verticesG,graph)
		print graph
		#break
	
	
	# graph.is_bipartite(True) regresa (True, [False, False, False, True, False, True, False, False, True, False, True, False, True, False, False])
	#donde el arreglo es de tamanio de vertices de la grafica, los true son vertices en un conjunto independiente

	return MISES

def llenarMatriz(vertices,graph):
	global matrizNoVecinos 

	matrizNoVecinos = []
	noVecinos = set()
	for v in vertices:
		vecinos=graph.neighbors(v)
		noVecinos = set(vertices) - set(vecinos)
		noVecinos.remove(v)
		matrizNoVecinos.append(list(noVecinos))

def obtenerMaxIguales(listaAnalizar, posMaxA):
	elemBuscar = listaAnalizar[posMaxA]
	listaMax = [posMaxA]
	for pos in range(len(listaAnalizar)):
		if listaAnalizar[pos] == elemBuscar and posMaxA != pos:
			#if pos not in valoresMaximos:
			listaMax.append(pos)

	return listaMax

def obtenerValoresMaximos(valoresObjetivo, valoresGeneral):
	maximos = []
	for pos in valoresObjetivo:
		maximos.append(valoresGeneral[pos])

	return maximos

def ordenarVecinosGrafo(grafoI, arregloVecinos):
	vecinosOrd = []
	gradoVecinos = []

	for v in arregloVecinos:
		try:
			gradoVecinos.append(grafoI.degree(v))
		except Exception, e:
			pass

	for i in gradoVecinos:
		vmaximo= max(gradoVecinos)
		posMaximo = gradoVecinos.index(vmaximo)

		vecinosOrd.append(arregloVecinos[posMaximo])

		gradoVecinos[posMaximo] = -1


	return vecinosOrd

def spanningTree(graph,arrayDFSF):
	sTF = Graph()
	tamanio= int(getTamanio(graph))

	sTF.add_vertices(tamanio)

	for posac in range(len(arrayDFSF)):
		nodo=arrayDFSF[posac]

		if posac == 0:
			posant=posac
		
		else:
			while verificaVecino(nodo,posant,graph,arrayDFSF) != 1:
				posant = posant - 1
			sTF.add_edges([(arrayDFSF[posant], nodo)])
			posant=posac

	return sTF

def verificaVecino(nodo,posante,graph,arrayDFS):
	vecinos=graph.neighbors(arrayDFS[posante])
	if nodo in vecinos:
		return 1
	else:
		return 0

def verificarCorteGrafo(puntoCorte,grafoVerificar):
	
	if puntoCorte in grafoVerificar.cut_vertices():
		return True
	else:
		return False
	# aristasGrafoSinNodo = obtenerListaGSinNodo(grafoVerificar, puntoCorte)

	# grafoVerificar1 = Graph(aristasGrafoSinNodo)
	

	# nodosGrafoVerificar1 = obtenerNodos(aristasGrafoSinNodo)
	
	# nodoVistados = []
	# grafosVC = []
	# posList = 0

	# while(posList < len(nodosGrafoVerificar1) - 1):

	# 	valorList = nodosGrafoVerificar1[posList]

	# 	'''Se verifica si el valor esta en los visitados, en caso de que no se toma como pivote y aplicar dfs '''
	# 	if valorList not in nodoVistados:
	# 		arrayDFS = []
	# 		for vertex in dfsiter(grafoVerificar1,valorList):
	# 			arrayDFS.append(vertex)
			
	# 		grafosVC.append(arrayDFS)

	# 		nodoVistados.extend([element for element in arrayDFS if element not in nodoVistados])
	# 		nodoVistados.sort()
		
		
		
	# 	posList = posList + 1

	
	
	# if len(grafosVC) > 1:
	# 	return 1
	# else:
	# 	return 0

def visualizarGrafo(graphI,nombres):
	grafo = Graph(graphI.get_edgelist())
	layout = grafo.layout("kk")
	grafo.vs["label"] = nombres

	grafo = eliminaNodosAislados(grafo)
	plot(grafo, layout = layout)

t1 = time.time()
#archivo='juego6.txt'
archivo = '/Users/angie-uaemex/Dropbox/Maestria/Coloreo/instanciasE2COL/Instancias/Pruebas/'+'inithx_i_2.txt'

g= igraph.Graph.Read_Edgelist(archivo,directed=False)  
aristasGrafo = g.get_edgelist()
verticesGrafo = getVertices(aristasGrafo)


#generarVerticesCompletos(verticesGrafo)
#visualizarGrafo(g,verticesCompletos)

subGrafos = getSubGrafos(g,verticesGrafo)

for arrayDFS in subGrafos:
	subGrafo = Graph(construirSubGrafo(g, arrayDFS))

	grafoDepurado, verticesGrafoDepurado = depurarGrafo(subGrafo, aristasGrafo, verticesGrafo, arrayDFS)


	llenarMatriz(verticesGrafoDepurado,grafoDepurado)

	MISES = generarMIS(grafoDepurado, verticesGrafoDepurado)
	print 'COLOREO: ',len(MISES) + 2
	print MISES

print(time.time() - t1)


